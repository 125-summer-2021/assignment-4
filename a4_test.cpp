// a4_test.cpp

//
// You can include any standard C++ files you like for testing purposes.
//
#include "a4.h"

using namespace std;

// converse is a helper function that you can use if you like. You are free to
// modify it in any way.
void converse(Chatbot* a, Chatbot* b, int max_turns = 50) {
  for(int turn = 1; turn <= max_turns; turn++) {
    string a_msg = a->get_msg();
    cout << "(" << turn << ") " << a->get_name() << ": " << a_msg << "\n";
    
    turn++;
    if (turn > max_turns) return;

    b->give_msg(a_msg);
    string b_msg = b->get_msg();

    cout << "(" << turn << ") " << b->get_name() << ": " << b_msg << "\n";
    a->give_msg(b_msg);
  } // for
}

//
// ... write your test code here ...
//

int main() {
  //
  // ... call your testing code here ...
  //
}
